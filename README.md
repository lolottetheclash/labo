Python : Exercices sur les sous-programmes et collections


1 Gestion d’un laboratoire de recherche

L’objectif de cet exercice est de proposer un outil de gestion pour un laboratoire de recherche. Pour simplifier, nous considérons que nous ne gérons qu’un seul laboratoire.

1.1 Version initiale
Dans un premier temps, on veut simplement recenser les personnes qui travaillent dans le laboratoire et, plus précisément, savoir quel bureau elles occupent.
On sait que :
-Chaque personne occupe un et un seul bureau.
-Un même bureau peut accueillir plusieurs personnes.
-Une personne est caractérisée par son seul nom. On suppose qu’il n’y a pas d’homonymes.
On veut donc pouvoir :
-enregistrer l’arrivée d’une nouvelle personne. On précise le nom de la personne et le bureau qu’elle occupe. Une exception PresentException signalera la tentative d’enregistrer une personne déjà connue du laboratoire.
-enregistrer le départ d’une personne. On indique le nom de la personne qui quitte le laboratoire (et donc libère son bureau). On lèvera une exception AbsentException si la personne est inconnue.
-modifier le bureau occupé par une personne. On indique le nom de la personne et son nouveau bureau.
-changer le nom d’une personne du laboratoire (en cas de mauvaise orthographe, marriage, divorce, etc.).
-savoir si une personne est membre du laboratoire.
-obtenir le bureau d’une personne.
-produire le listing de tous les personnels avec le bureau occupé.

Un menu textuel doit donner accès à ces différentes possibilités.

Programmer l’application. On utilisera pytest pour tester au fur et à mesure.
On définira ainsi les exceptions susmentionnées :

class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass

class AbsentException(LaboException):
    pass

class PresentException(LaboException):
    pass


1.2 Occupation des bureaux

Une nouvelle fonctionnalité est demandée : la possibilité d’afficher l’occupation des bureaux.
On affichera d’abord d’occupation des bureaux en mode texte. Voici le type d’affichage attendu :
F305:
- Marc
- Xavier
F307:
-Aurélie

On produira l’occupation des bureaux sous la forme d’une page HTML
On affichera les bureaux et leurs occupnants dans l’ordre lexicographique 

1.4 Gestion de la persistence

Entre deux exécutions de l’application, on veut conserver les informations déjà renseignées. Pour ce faire un sauvegardera dans un ou plusieurs fichiers texte les informations de manière à pouvoir les charger à chaque lancement de l’application.
On pourra utiliser le format JSON et le module json de Python.
On veut aussi pouvoir importer dans le laboratoire des données au format csv. Voici un exemple d’un tel fichier (la première ligne contient le titre des colonnes).

Nom, Bureau
Aurélie, F307
Xavier, F305
Marc, F305

On utilisera le module csv et en particulier DictReader. 
On affichera à la fin un rapport pour indiquer les personnes du fichiers csv déjà enregistrées mais avec un numéro de bureau différent.
