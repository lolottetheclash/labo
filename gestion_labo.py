from labo import *
from menu import *
import re 

def gestion_labo():

    regex = re.compile('[@_!#$%^&*()<>?/\|}{~:0-9]') # vérifie que le nom est bien entrée, sans caractères spéciaux ni chiffres

    def commande_arrivee():
        try: 
            nom: str = input("Merci d'entrer le nom à enregistrer:")
            assert regex.search(nom) == None
            bureau: str = input("Merci d'entrer le bureau associé:")
            enregistrer_arrivee(l, nom, bureau)
        except PresentException: 
            print("Cette personne est déjà inscrite. Merci d'ajouter une autre personne.")
        except AssertionError:
            print("Merci d'entrer un nom valide.\n")


    def commande_depart():
        try:
            nom: str = input("Merci d'entrer le nom de la personne à retirer du laboratoire:")
            assert regex.search(nom) == None
            enregistrer_depart(l, nom)
        except AbsentException:
            print("Cette personne n'est pas inscrite dans le laboratoire. Merci d'entre un autre nom.")
        except AssertionError:
            print("Merci d'entrer un nom valide.\n")


    def commande_modifier_bureau():
        try:
            nom: str = input("Merci d'entrer le nom de la personne qui change de bureau:")
            assert regex.search(nom) == None
            bureau: str = input("Merci d'entrer son nouveau bureau:")
            modifier_bureau(l, nom, bureau)
        except AbsentException:
            print("Cette personne n'est pas inscrite dans le laboratoire. Merci d'entre un autre nom.")
        except AssertionError:
            print("Merci d'entrer un nom valide.\n")


    def commande_modifier_nom():
        try:
            nom: str = input("Merci d'entrer le nom de la personne que vous désirez corriger:")
            nouveau_nom: str = input("Merci d'entrer son nom corrigé:")
            assert regex.search(nom) == None 
            assert regex.search(nouveau_nom) == None
            modifier_nom(l, nom, nouveau_nom)
        except AbsentException:
            print("Cette personne n'est pas inscrite dans le laboratoire. Merci d'entre un autre nom.")
        except AssertionError:
            print("Merci d'entrer un nom et nouveau nom valide.\n")


    def commande_est_membre():
        try:
            nom: str = input("Merci d'entrer le nom de la personne pour vérifier si elle est membre du laboratoire:")
            assert regex.search(nom) == None
            if est_membre(l, nom):
                print(f'{nom} est membre du labo.')
            else:
                print(f"{nom} est n'est pas membre du labo.")
        except AssertionError:
            print("Merci d'entrer un nom valide.\n")


    def commande_quel_bureau():
        try:
            nom: str = input("Merci d'entrer le nom de la personne pour connaître son bureau associé:")
            assert regex.search(nom) == None
            bureau = quel_bureau(l, nom)
            print(f'{nom} se trouve au bureau {bureau}.')
        except AbsentException:
            print("Cette personne n'est pas inscrite dans le laboratoire. Merci d'entrer un autre nom.")
        except AssertionError:
            print("Merci d'entrer un nom valide.\n")


    def commande_listing():
        listing(l)


    def commande_occupation_bureaux_texte():
        afficher_occupation_bureaux(l)
    

    def commande_occupation_bureaux_html():
        occupation_bureaux_html(l, 'labo.html')
    
    
    def commande_occupation_bureaux_csv():
        occupation_bureaux_csv(l,'labo.csv')


    def commande_quitter():
        print("Vous avez choisi de sortir du programme. A bientôt!")

    
    l = labo()

    m = menu()

    ajouter_choix(m, "Quitter.", commande_quitter)
    ajouter_choix(m, "Enregistrer l’arrivée d’une nouvelle personne.", commande_arrivee)
    ajouter_choix(m, "Enregistrer le départ d’une personne.", commande_depart)
    ajouter_choix(m, "Modifier le bureau occupé par une personne.", commande_modifier_bureau)
    ajouter_choix(m, "Changer le nom d’une personne.", commande_modifier_nom)
    ajouter_choix(m, "Savoir si une personne est membre du laboratoire.", commande_est_membre)
    ajouter_choix(m, "Obtenir le bureau d’une personne.", commande_quel_bureau)
    ajouter_choix(m, "Produire le listing de tous les personnels avec le bureau occupé.", commande_listing)
    ajouter_choix(m, "Afficher l’occupation des bureaux texte.", commande_occupation_bureaux_texte)
    ajouter_choix(m, "Afficher l’occupation des bureaux html.", commande_occupation_bureaux_html)
    ajouter_choix(m, "Importer des données via csv.", commande_occupation_bureaux_csv)

    l = ouvrir_json("labo.json")
    gerer_menu(m,l)