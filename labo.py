from collections import OrderedDict # pour conserver l'ordre de tri dans le dictionnaire
import json
import csv

'''gestion du labo'''
class LaboException(Exception):
    """Généralise les exceptions du laboratoire"""
    pass
class AbsentException(LaboException):
    pass
class PresentException(LaboException):
    pass


def labo():
    mon_labo = dict()
    return mon_labo


def taille(labo):
    return len(labo)


def ouvrir_json(fichier):
    with open(fichier, "r") as fichier_ext:
        labo = json.load(fichier_ext)
        return labo


def fermer_json(fichier, labo):
    with open(fichier, "w") as fichier_ext:
        json.dump(labo, fichier_ext, indent=4)


def enregistrer_arrivee(labo, nom, bureau):
    """la personne pourrait être déjà enregistrée dans le bureau"""
    if nom in labo:
        raise PresentException
    labo[nom] = bureau
    fermer_json("labo.json", labo)


def enregistrer_depart(labo, nom):
    """la personne pourrait ne pas être dans le labo"""
    if nom not in labo:
        raise AbsentException
    labo.pop(nom)
    fermer_json("labo.json", labo)


def modifier_bureau(labo, nom, bureau):
    if nom not in labo:
        raise AbsentException
    labo[nom] = bureau
    fermer_json("labo.json", labo)


def modifier_nom(labo, nom, nouveau_nom):
    if nom not in labo:
        raise AbsentException
    labo[nouveau_nom] = labo[nom]
    del labo[nom]
    fermer_json("labo.json", labo)


def est_membre(labo, nom):
    return (nom in labo)
    

def quel_bureau(labo, nom):
    if nom not in labo:
        raise AbsentException
    return labo[nom]


def listing(labo):
    print("Voici le contenu du laboratoire:\n")
    for key, value in labo.items():
        print(f'{key} se trouve dans le bureau {value}.')
    print('')


def occupation_bureaux_texte(labo):
    bureaux = {}
    for key,value in labo.items():
        bureaux.setdefault(value, []).append(key)
    bureaux = OrderedDict(sorted(bureaux.items()))
    return bureaux

def afficher_occupation_bureaux(l):
    bureaux = occupation_bureaux_texte(l)
    for key,value in bureaux.items():
        print(f'{key}:')
        for val in sorted(value):
            print(f'- {val}')


def occupation_bureaux_html(l, fichier):
    debut = '''
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Descritif de la page">
    <title>Bureaux</title>
</head>

<body>
    <h1>Liste des bureaux occupés du laboratoire.</h1>'''
    fin = '''
</body>
</html>'''
    bureaux = occupation_bureaux_texte(l)
    with open(fichier, 'w') as fichier:
        fichier.write(debut)
        for key,value in bureaux.items():
            fichier.write(f'<p>{key}<p>')
            for val in sorted(value):
                fichier.write(f'<p>- {val}<p>')
        fichier.write(fin)


def occupation_bureaux_csv(labo, fichier):
    """On affichera à la fin un rapport pour indiquer les personnes du fichiers csv déjà enregistrées mais avec un numéro de bureau différent"""
    with open(fichier, newline='') as myFile:
        reader = csv.DictReader(myFile)
        for row in reader:
            if row['Nom'] in labo:
                print("{} était déjà enregistré chez nous et était associé au bureau {}.\n Il n'a donc pas été rajouté.".format(row['Nom'], labo[row['Nom']]))
            else:
                labo[row['Nom']] = row['Bureau']
    fermer_json("labo.json", labo)





