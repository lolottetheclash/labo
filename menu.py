def menu():
    return []

def ajouter_choix(menu, intitule, commande):
    menu.append((intitule, commande))

def _afficher_menu(m):
    for num, entree in enumerate(m, 0): # entree =(intitule, commande)
        intitule, _ = entree # 
        print(num, intitule)

def _input_choix(m):
    while True:
        try:
            choix: int = int(input("Que choisissez-vous?\n"))
            assert choix >= 0 and choix <= len(m)-1
            return choix 
        except ValueError:
            print("Merci d'entrer un entier.")
        except AssertionError:
            print(f'Votre choix doit être compris entre 0 et {len(m)-1}.')


def _traiter_choix(m,choix):
  m[choix][1]()
        
            
def gerer_menu(m,l):
    choix = None
    while choix != 0:
        _afficher_menu(m) 
        choix = _input_choix(m)
        _traiter_choix(m, choix)


# m = [(enregistrer enregistrer_arrivee), (intitule2, commnde2)........]
# pour lancer la commande1 qui est enregistrer_arrivee() qui correspond au choix 1 du user:
#  il faut aller chercher dans le menu m donc choisir le 1er tuple soit m[choix -1] = choix du user = 1, 
# le tuple qui ns intéresse est à l'index 0 du menu donc on fait choix -1
# une fois le tuple ciblé, on va chercher la commande qui se trouve dans le tuple, à l'index 1 du tuple
# donc on fait [1]
# enfin, on y ajoute les () pour que cela lance la commande, ce qui donne:
# m[choix - 1][1]() ...........=> entrée = m[choix-1] /commande = entree[1] / commande() => appel de la def correspondante
