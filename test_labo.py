'''Programme de test de labo.'''
import pytest
from labo import *
from gestion_labo import *

'''on crée nos variables à exploiter pour le test'''

@pytest.fixture
def l():
    return {
    "lolo": "X80",
    "nathie": "A70",
    "ol": "I5",
    "Aur\u00e9lie": "F307",
    "Xavier": "F305",
    "Marc": "F305",
    "lammmme": "B70"
    }

@pytest.fixture
def p():
    return "steph"

@pytest.fixture
def b():
    return "F100"

'''on lance tous les tests sur toutes nos defs'''

def test_labo():
    '''Tests de création d'un labo'''
    assert dict() == labo()

def test_taille(l):
    '''Test de la fonction taille d'un labo'''
    assert len(l) == 7


def test_enregistrer_arrivee(l, p, b):
    '''Test de l'enregistrement d'une arrivée'''
    enregistrer_arrivee(l, p, b)
    assert len(l) == 8

def test_enregistrer_arrivee_erreur(l, p, b):
    '''Doit soulever une erreur si personne déjà inscrite'''
    enregistrer_arrivee(l, p, b)
    with pytest.raises(PresentException):
        enregistrer_arrivee(l, p, b)

def test_enregistrer_depart(l):
    '''Test de l'enregistrement d'un départ'''
    enregistrer_depart(l, "lolo")
    len(l) == 7

def test_enregistrer_depart_erreur(l, p, b):
    '''Doit soulever une erreur si personne n'existe pas'''
    with pytest.raises(AbsentException):
        enregistrer_depart(l, p)








